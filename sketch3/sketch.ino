const int led1 = 9;
const int llave1 = 4;
int tension;

void setup() {
	pinMode(led1, OUTPUT);
	pinMode(llave1, INPUT);
}

void loop() {
	tension = digitalRead(llave1);
	if ( tension == HIGH ) {
		digitalWrite(led1, HIGH);
	}
	else {
		digitalWrite(led1, LOW);
	}
}